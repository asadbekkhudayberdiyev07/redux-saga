import { useDispatch, useSelector } from "react-redux";
import { getUsersFetch } from "./actions";

function App() {
  const dispatch = useDispatch();
  const users = useSelector((state) => state.MyFirstReducer.users);
  return (
    <div className="App">
      <button onClick={() => dispatch(getUsersFetch())}>Get users</button>
      <div>Users: {users && users?.map((user) => <div>{user?.name}</div>)}</div>
    </div>
  );
}

export default App;
